package applicationpackage;

import vehiclespackage.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("coolBikes", 13, 54.0);
        bikes[1] = new Bicycle("BikersU", 23, 56.5);
        bikes[2] = new Bicycle("WeBike", 8, 34.0);
        bikes[3] = new Bicycle("UtraBike", 21, 114.0);

        for (int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i]);
        }
    }
}
